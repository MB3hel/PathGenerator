///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/listctrl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/frame.h>
#include <wx/textctrl.h>
#include <wx/notebook.h>
#include <wx/dialog.h>
#include <wx/scrolwin.h>
#include <wx/combobox.h>
#include <wx/choice.h>
#include <wx/button.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class PreviewWindowBase
///////////////////////////////////////////////////////////////////////////////
class PreviewWindowBase : public wxFrame
{
	private:

	protected:
		wxListCtrl* pathList;
		wxPanel* rootPanel;
		wxPanel* borderColor;
		wxStaticText* borderLabel;
		wxPanel* midlineColor;
		wxStaticText* midlineLabel;
		wxPanel* reflineColor;
		wxStaticText* reflineLabel;
		wxPanel* pathColor;
		wxStaticText* pathLabel;
		wxMenuBar* menubar;
		wxMenu* fileMenu;
		wxMenuItem* mnuRefresh;
		wxMenu* viewMenu;
		wxMenuItem* mnuShowBorders;
		wxMenuItem* mnuShowMidlines;
		wxMenuItem* mnuShowRefLines;
		wxMenu* pathMenu;

		// Virtual event handlers, overide them in your derived class
		virtual void onPaint( wxPaintEvent& event ) { event.Skip(); }
		virtual void loadDataFile( wxCommandEvent& event ) { event.Skip(); }
		virtual void refreshDatafile( wxCommandEvent& event ) { event.Skip(); }
		virtual void aboutSelected( wxCommandEvent& event ) { event.Skip(); }
		virtual void drawnItemsChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void savePaths( wxCommandEvent& event ) { event.Skip(); }


	public:

		PreviewWindowBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Path Generator Preview"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 750,450 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~PreviewWindowBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class AboutDialogBase
///////////////////////////////////////////////////////////////////////////////
class AboutDialogBase : public wxDialog
{
	private:

	protected:
		wxStaticText* AboutLabel;
		wxStaticText* versionLabel;
		wxNotebook* DisplayNotebook;
		wxPanel* LicensePanel;
		wxStaticText* LicenseLabel;
		wxTextCtrl* LicenseTextView;
		wxPanel* wxwPanel;
		wxStaticText* wxwLabel;
		wxTextCtrl* wxwReadmeView;
		wxPanel* pathfinderPanel;
		wxStaticText* pathfinderLabel;
		wxTextCtrl* pathfinderView;
		wxPanel* yamlPanel;
		wxStaticText* yamlLabel;
		wxTextCtrl* yamlLicneseView;

	public:

		AboutDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("About Path Generator"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_DIALOG_STYLE );
		~AboutDialogBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DatafileGUIBase
///////////////////////////////////////////////////////////////////////////////
class DatafileGUIBase : public wxFrame
{
	private:

	protected:
		wxScrolledWindow* mainContent;
		wxBoxSizer* scrollContentSizer;

	public:

		DatafileGUIBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~DatafileGUIBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class RefLineViewBase
///////////////////////////////////////////////////////////////////////////////
class RefLineViewBase : public wxPanel
{
	private:

	protected:
		wxStaticText* blankLabel;
		wxStaticText* x1Label;
		wxStaticText* y1Label;
		wxStaticText* x2Label;
		wxStaticText* y2Label;
		wxStaticText* lengthLabel;
		wxComboBox* x1Value;
		wxComboBox* y1Value;
		wxComboBox* lengthValue;
		wxComboBox* x2Value;
		wxComboBox* y2Value;

		// Virtual event handlers, overide them in your derived class
		virtual void typeSelected( wxCommandEvent& event ) { event.Skip(); }


	public:
		wxComboBox* refLineType;

		RefLineViewBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 750,100 ), long style = wxTAB_TRAVERSAL, const wxString& name = wxEmptyString );
		~RefLineViewBase();

};

///////////////////////////////////////////////////////////////////////////////
/// Class SimulateDialogBase
///////////////////////////////////////////////////////////////////////////////
class SimulateDialogBase : public wxDialog
{
	private:

	protected:
		wxStaticText* lblSImulation;
		wxChoice* choiceSelector;
		wxStaticText* lblMode;
		wxChoice* choicePfMode;
		wxStaticText* lblPath;
		wxChoice* choicePath;
		wxButton* btnStartSimulate;

	public:

		SimulateDialogBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Simulate"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 350,-1 ), long style = wxDEFAULT_DIALOG_STYLE );
		~SimulateDialogBase();

};

