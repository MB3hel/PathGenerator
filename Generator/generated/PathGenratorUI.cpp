///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "PathGenratorUI.h"

///////////////////////////////////////////////////////////////////////////

PreviewWindowBase::PreviewWindowBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* verticalSizer;
	verticalSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* horizontalSizer;
	horizontalSizer = new wxBoxSizer( wxHORIZONTAL );

	wxStaticBoxSizer* pathListSizer;
	pathListSizer = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Paths") ), wxVERTICAL );

	pathList = new wxListCtrl( pathListSizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 200,-1 ), wxLC_NO_HEADER|wxLC_REPORT|wxLC_SINGLE_SEL );
	pathListSizer->Add( pathList, 1, wxALL|wxEXPAND, 5 );


	horizontalSizer->Add( pathListSizer, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* rootLabelSizer;
	rootLabelSizer = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Preview") ), wxVERTICAL );

	rootPanel = new wxPanel( rootLabelSizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	rootPanel->SetBackgroundColour( wxColour( 255, 255, 255 ) );

	rootLabelSizer->Add( rootPanel, 1, wxEXPAND, 5 );


	horizontalSizer->Add( rootLabelSizer, 1, wxALL|wxEXPAND, 5 );


	verticalSizer->Add( horizontalSizer, 1, wxEXPAND, 5 );

	wxStaticBoxSizer* keySizer;
	keySizer = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Key") ), wxVERTICAL );

	wxBoxSizer* keyVSizer;
	keyVSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* topRowSizer;
	topRowSizer = new wxBoxSizer( wxHORIZONTAL );

	borderColor = new wxPanel( keySizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 50,-1 ), wxBORDER_SIMPLE|wxTAB_TRAVERSAL );
	borderColor->SetBackgroundColour( wxColour( 0, 0, 0 ) );

	topRowSizer->Add( borderColor, 0, wxEXPAND | wxALL, 5 );

	borderLabel = new wxStaticText( keySizer->GetStaticBox(), wxID_ANY, wxT("Region Borders"), wxDefaultPosition, wxDefaultSize, 0 );
	borderLabel->Wrap( -1 );
	topRowSizer->Add( borderLabel, 1, wxALL, 5 );

	midlineColor = new wxPanel( keySizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 50,-1 ), wxBORDER_SIMPLE|wxTAB_TRAVERSAL );
	midlineColor->SetBackgroundColour( wxColour( 151, 151, 151 ) );

	topRowSizer->Add( midlineColor, 0, wxEXPAND | wxALL, 5 );

	midlineLabel = new wxStaticText( keySizer->GetStaticBox(), wxID_ANY, wxT("Region Midlines"), wxDefaultPosition, wxDefaultSize, 0 );
	midlineLabel->Wrap( -1 );
	topRowSizer->Add( midlineLabel, 1, wxALL, 5 );


	keyVSizer->Add( topRowSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* bottomRowSizer;
	bottomRowSizer = new wxBoxSizer( wxHORIZONTAL );

	reflineColor = new wxPanel( keySizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 50,-1 ), wxBORDER_SIMPLE|wxTAB_TRAVERSAL );
	reflineColor->SetBackgroundColour( wxColour( 255, 128, 0 ) );

	bottomRowSizer->Add( reflineColor, 0, wxALL|wxEXPAND, 5 );

	reflineLabel = new wxStaticText( keySizer->GetStaticBox(), wxID_ANY, wxT("Reference Lines"), wxDefaultPosition, wxDefaultSize, 0 );
	reflineLabel->Wrap( -1 );
	bottomRowSizer->Add( reflineLabel, 1, wxALL, 5 );

	pathColor = new wxPanel( keySizer->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 50,-1 ), wxBORDER_SIMPLE|wxTAB_TRAVERSAL );
	pathColor->SetBackgroundColour( wxColour( 0, 255, 0 ) );

	bottomRowSizer->Add( pathColor, 0, wxEXPAND | wxALL, 5 );

	pathLabel = new wxStaticText( keySizer->GetStaticBox(), wxID_ANY, wxT("Paths and Waypoints"), wxDefaultPosition, wxDefaultSize, 0 );
	pathLabel->Wrap( -1 );
	bottomRowSizer->Add( pathLabel, 1, wxALL, 5 );


	keyVSizer->Add( bottomRowSizer, 1, wxEXPAND, 5 );


	keySizer->Add( keyVSizer, 1, wxEXPAND, 5 );


	verticalSizer->Add( keySizer, 0, wxEXPAND, 5 );


	this->SetSizer( verticalSizer );
	this->Layout();
	menubar = new wxMenuBar( 0 );
	fileMenu = new wxMenu();
	wxMenuItem* mnuLoadDatafile;
	mnuLoadDatafile = new wxMenuItem( fileMenu, wxID_ANY, wxString( wxT("Load Datafile") ) , wxEmptyString, wxITEM_NORMAL );
	fileMenu->Append( mnuLoadDatafile );

	mnuRefresh = new wxMenuItem( fileMenu, wxID_ANY, wxString( wxT("Refresh Datafile") ) , wxEmptyString, wxITEM_NORMAL );
	fileMenu->Append( mnuRefresh );
	mnuRefresh->Enable( false );

	wxMenuItem* mnuAbout;
	mnuAbout = new wxMenuItem( fileMenu, wxID_ANY, wxString( wxT("About Path Generator") ) , wxEmptyString, wxITEM_NORMAL );
	fileMenu->Append( mnuAbout );

	menubar->Append( fileMenu, wxT("File") );

	viewMenu = new wxMenu();
	mnuShowBorders = new wxMenuItem( viewMenu, wxID_ANY, wxString( wxT("Show Borders") ) , wxEmptyString, wxITEM_CHECK );
	viewMenu->Append( mnuShowBorders );
	mnuShowBorders->Check( true );

	mnuShowMidlines = new wxMenuItem( viewMenu, wxID_ANY, wxString( wxT("Show Midlines") ) , wxEmptyString, wxITEM_CHECK );
	viewMenu->Append( mnuShowMidlines );
	mnuShowMidlines->Check( true );

	mnuShowRefLines = new wxMenuItem( viewMenu, wxID_ANY, wxString( wxT("Show Ref Lines") ) , wxEmptyString, wxITEM_CHECK );
	viewMenu->Append( mnuShowRefLines );
	mnuShowRefLines->Check( true );

	menubar->Append( viewMenu, wxT("View") );

	pathMenu = new wxMenu();
	wxMenuItem* mnuSavePaths;
	mnuSavePaths = new wxMenuItem( pathMenu, wxID_ANY, wxString( wxT("Save Paths") ) , wxEmptyString, wxITEM_NORMAL );
	pathMenu->Append( mnuSavePaths );

	menubar->Append( pathMenu, wxT("Paths") );

	this->SetMenuBar( menubar );


	this->Centre( wxBOTH );

	// Connect Events
	rootPanel->Connect( wxEVT_PAINT, wxPaintEventHandler( PreviewWindowBase::onPaint ), NULL, this );
	fileMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::loadDataFile ), this, mnuLoadDatafile->GetId());
	fileMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::refreshDatafile ), this, mnuRefresh->GetId());
	fileMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::aboutSelected ), this, mnuAbout->GetId());
	viewMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::drawnItemsChanged ), this, mnuShowBorders->GetId());
	viewMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::drawnItemsChanged ), this, mnuShowMidlines->GetId());
	viewMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::drawnItemsChanged ), this, mnuShowRefLines->GetId());
	pathMenu->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( PreviewWindowBase::savePaths ), this, mnuSavePaths->GetId());
}

PreviewWindowBase::~PreviewWindowBase()
{
	// Disconnect Events
	rootPanel->Disconnect( wxEVT_PAINT, wxPaintEventHandler( PreviewWindowBase::onPaint ), NULL, this );

}

AboutDialogBase::AboutDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* DialogSizer;
	DialogSizer = new wxBoxSizer( wxVERTICAL );

	AboutLabel = new wxStaticText( this, wxID_ANY, wxT("Flexible Motion Profile Path Generator for FRC using Pathfinder"), wxDefaultPosition, wxDefaultSize, 0 );
	AboutLabel->Wrap( -1 );
	AboutLabel->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );

	DialogSizer->Add( AboutLabel, 0, wxALL, 5 );

	versionLabel = new wxStaticText( this, wxID_ANY, wxT("VERSION HERE"), wxDefaultPosition, wxDefaultSize, 0 );
	versionLabel->Wrap( -1 );
	versionLabel->SetFont( wxFont( 6, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );

	DialogSizer->Add( versionLabel, 0, wxALL, 5 );

	wxBoxSizer* DisplayPanelSizer;
	DisplayPanelSizer = new wxBoxSizer( wxHORIZONTAL );


	DialogSizer->Add( DisplayPanelSizer, 1, wxEXPAND, 5 );

	DisplayNotebook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	LicensePanel = new wxPanel( DisplayNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* LicensePanelSizer;
	LicensePanelSizer = new wxBoxSizer( wxVERTICAL );

	LicenseLabel = new wxStaticText( LicensePanel, wxID_ANY, wxT("Path Generator is licensed under the MIT license"), wxDefaultPosition, wxDefaultSize, 0 );
	LicenseLabel->Wrap( -1 );
	LicensePanelSizer->Add( LicenseLabel, 0, wxALL, 5 );

	LicenseTextView = new wxTextCtrl( LicensePanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxTE_MULTILINE|wxTE_READONLY );
	LicensePanelSizer->Add( LicenseTextView, 1, wxALL|wxEXPAND, 5 );


	LicensePanel->SetSizer( LicensePanelSizer );
	LicensePanel->Layout();
	LicensePanelSizer->Fit( LicensePanel );
	DisplayNotebook->AddPage( LicensePanel, wxT("License"), true );
	wxwPanel = new wxPanel( DisplayNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* wxwPanelSizer;
	wxwPanelSizer = new wxBoxSizer( wxVERTICAL );

	wxwLabel = new wxStaticText( wxwPanel, wxID_ANY, wxT("wxWidgets used for GUI."), wxDefaultPosition, wxDefaultSize, 0 );
	wxwLabel->Wrap( -1 );
	wxwPanelSizer->Add( wxwLabel, 0, wxALL, 5 );

	wxwReadmeView = new wxTextCtrl( wxwPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxTE_MULTILINE|wxTE_READONLY );
	wxwPanelSizer->Add( wxwReadmeView, 1, wxALL|wxEXPAND, 5 );


	wxwPanel->SetSizer( wxwPanelSizer );
	wxwPanel->Layout();
	wxwPanelSizer->Fit( wxwPanel );
	DisplayNotebook->AddPage( wxwPanel, wxT("wxWidgets"), false );
	pathfinderPanel = new wxPanel( DisplayNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* pathfinderPanelSizer;
	pathfinderPanelSizer = new wxBoxSizer( wxVERTICAL );

	pathfinderLabel = new wxStaticText( pathfinderPanel, wxID_ANY, wxT("Pathfinder used to generate paths."), wxDefaultPosition, wxDefaultSize, 0 );
	pathfinderLabel->Wrap( -1 );
	pathfinderPanelSizer->Add( pathfinderLabel, 0, wxALL, 5 );

	pathfinderView = new wxTextCtrl( pathfinderPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxTE_MULTILINE|wxTE_READONLY );
	pathfinderPanelSizer->Add( pathfinderView, 1, wxALL|wxEXPAND, 5 );


	pathfinderPanel->SetSizer( pathfinderPanelSizer );
	pathfinderPanel->Layout();
	pathfinderPanelSizer->Fit( pathfinderPanel );
	DisplayNotebook->AddPage( pathfinderPanel, wxT("Pathfinder"), false );
	yamlPanel = new wxPanel( DisplayNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* yamlPanelSizer;
	yamlPanelSizer = new wxBoxSizer( wxVERTICAL );

	yamlLabel = new wxStaticText( yamlPanel, wxID_ANY, wxT("YAML-CPP is used to parse YAML datafiles."), wxDefaultPosition, wxDefaultSize, 0 );
	yamlLabel->Wrap( -1 );
	yamlPanelSizer->Add( yamlLabel, 0, wxALL, 5 );

	yamlLicneseView = new wxTextCtrl( yamlPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxTE_MULTILINE|wxTE_READONLY );
	yamlPanelSizer->Add( yamlLicneseView, 1, wxALL|wxEXPAND, 5 );


	yamlPanel->SetSizer( yamlPanelSizer );
	yamlPanel->Layout();
	yamlPanelSizer->Fit( yamlPanel );
	DisplayNotebook->AddPage( yamlPanel, wxT("YAML"), false );

	DialogSizer->Add( DisplayNotebook, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( DialogSizer );
	this->Layout();

	this->Centre( wxBOTH );
}

AboutDialogBase::~AboutDialogBase()
{
}

DatafileGUIBase::DatafileGUIBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* datafileEditorMainSizer;
	datafileEditorMainSizer = new wxBoxSizer( wxVERTICAL );

	mainContent = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	mainContent->SetScrollRate( 5, 5 );
	scrollContentSizer = new wxBoxSizer( wxVERTICAL );


	mainContent->SetSizer( scrollContentSizer );
	mainContent->Layout();
	scrollContentSizer->Fit( mainContent );
	datafileEditorMainSizer->Add( mainContent, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( datafileEditorMainSizer );
	this->Layout();

	this->Centre( wxBOTH );
}

DatafileGUIBase::~DatafileGUIBase()
{
}

RefLineViewBase::RefLineViewBase( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name ) : wxPanel( parent, id, pos, size, style, name )
{
	wxBoxSizer* mainContent;
	mainContent = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* labelSizer;
	labelSizer = new wxBoxSizer( wxHORIZONTAL );

	blankLabel = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	blankLabel->Wrap( -1 );
	labelSizer->Add( blankLabel, 1, wxALL, 5 );

	x1Label = new wxStaticText( this, wxID_ANY, wxT("x1"), wxDefaultPosition, wxDefaultSize, 0 );
	x1Label->Wrap( -1 );
	labelSizer->Add( x1Label, 1, wxALL, 5 );

	y1Label = new wxStaticText( this, wxID_ANY, wxT("y1"), wxDefaultPosition, wxDefaultSize, 0 );
	y1Label->Wrap( -1 );
	labelSizer->Add( y1Label, 1, wxALL, 5 );

	x2Label = new wxStaticText( this, wxID_ANY, wxT("x2"), wxDefaultPosition, wxDefaultSize, 0 );
	x2Label->Wrap( -1 );
	labelSizer->Add( x2Label, 1, wxALL, 5 );

	y2Label = new wxStaticText( this, wxID_ANY, wxT("y2"), wxDefaultPosition, wxDefaultSize, 0 );
	y2Label->Wrap( -1 );
	labelSizer->Add( y2Label, 1, wxALL, 5 );

	lengthLabel = new wxStaticText( this, wxID_ANY, wxT("length"), wxDefaultPosition, wxDefaultSize, 0 );
	lengthLabel->Wrap( -1 );
	labelSizer->Add( lengthLabel, 1, wxALL, 5 );


	mainContent->Add( labelSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* controlSizer;
	controlSizer = new wxBoxSizer( wxHORIZONTAL );

	refLineType = new wxComboBox( this, wxID_ANY, wxT("Horizontal"), wxDefaultPosition, wxDefaultSize, 0, NULL, wxCB_DROPDOWN|wxCB_READONLY );
	refLineType->Append( wxT("Horizontal") );
	refLineType->Append( wxT("Vertical") );
	refLineType->Append( wxT("Custom") );
	refLineType->SetSelection( 0 );
	controlSizer->Add( refLineType, 1, wxALL, 5 );

	x1Value = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	controlSizer->Add( x1Value, 1, wxALL, 5 );

	y1Value = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	controlSizer->Add( y1Value, 1, wxALL, 5 );

	lengthValue = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), 0, NULL, 0 );
	controlSizer->Add( lengthValue, 1, wxALL, 5 );

	x2Value = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	controlSizer->Add( x2Value, 1, wxALL, 5 );

	y2Value = new wxComboBox( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	controlSizer->Add( y2Value, 1, wxALL, 5 );


	mainContent->Add( controlSizer, 0, wxEXPAND, 5 );


	this->SetSizer( mainContent );
	this->Layout();

	// Connect Events
	refLineType->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( RefLineViewBase::typeSelected ), NULL, this );
}

RefLineViewBase::~RefLineViewBase()
{
	// Disconnect Events
	refLineType->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( RefLineViewBase::typeSelected ), NULL, this );

}

SimulateDialogBase::SimulateDialogBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* rootSimulateSizer;
	rootSimulateSizer = new wxBoxSizer( wxVERTICAL );

	lblSImulation = new wxStaticText( this, wxID_ANY, wxT("Simulation Type"), wxDefaultPosition, wxDefaultSize, 0 );
	lblSImulation->Wrap( -1 );
	rootSimulateSizer->Add( lblSImulation, 0, wxALL|wxEXPAND, 5 );

	wxArrayString choiceSelectorChoices;
	choiceSelector = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, choiceSelectorChoices, 0 );
	choiceSelector->SetSelection( 0 );
	rootSimulateSizer->Add( choiceSelector, 0, wxALL|wxEXPAND, 5 );

	lblMode = new wxStaticText( this, wxID_ANY, wxT("Pathfinder Mode"), wxDefaultPosition, wxDefaultSize, 0 );
	lblMode->Wrap( -1 );
	rootSimulateSizer->Add( lblMode, 0, wxALL|wxEXPAND, 5 );

	wxArrayString choicePfModeChoices;
	choicePfMode = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, choicePfModeChoices, 0 );
	choicePfMode->SetSelection( 0 );
	rootSimulateSizer->Add( choicePfMode, 0, wxALL|wxEXPAND, 5 );

	lblPath = new wxStaticText( this, wxID_ANY, wxT("Path"), wxDefaultPosition, wxDefaultSize, 0 );
	lblPath->Wrap( -1 );
	rootSimulateSizer->Add( lblPath, 0, wxALL|wxEXPAND, 5 );

	wxArrayString choicePathChoices;
	choicePath = new wxChoice( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, choicePathChoices, 0 );
	choicePath->SetSelection( 0 );
	rootSimulateSizer->Add( choicePath, 0, wxALL|wxEXPAND, 5 );

	btnStartSimulate = new wxButton( this, wxID_ANY, wxT("Start Simulation"), wxDefaultPosition, wxDefaultSize, 0 );
	rootSimulateSizer->Add( btnStartSimulate, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( rootSimulateSizer );
	this->Layout();

	this->Centre( wxBOTH );
}

SimulateDialogBase::~SimulateDialogBase()
{
}
