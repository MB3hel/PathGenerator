#include <DataManager.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <mutex>

#include <Vector2.hpp>

using namespace datafile;

////////////////////////////////////////////////////
/// Exceptions
////////////////////////////////////////////////////
ParseException::ParseException(std::string errorMessage) : errorMessage(errorMessage) {}

const char *ParseException::what() const throw(){
	return errorMessage.c_str();
}

types::NoSuchKeyException::NoSuchKeyException(std::string errorMessage) : errorMessage(errorMessage){}

const char *types::NoSuchKeyException::what() const throw(){
	return errorMessage.c_str();
}

types::NoSuchItemException::NoSuchItemException(std::string errorMessage) : errorMessage(errorMessage){}

const char *types::NoSuchItemException::what() const throw(){
	return errorMessage.c_str();
}

////////////////////////////////////////////////////
/// ReferenceLine
////////////////////////////////////////////////////
double ReferenceLine::getStartX(LoadedData &data){
	return data.getDimmension(x1) + data.getDimmension(x1Offset);
}

double ReferenceLine::getStartY(LoadedData &data){
	return data.getDimmension(y1) + data.getDimmension(y1Offset);
}

double ReferenceLine::getEndX(LoadedData &data){
	if(oriented){
		if(types::getType(types::orientations, orientation) == types::Orientation::Horizontal){
			return data.getDimmension(x1) + data.getDimmension(length) + data.getDimmension(x1Offset);
		}else{
			return data.getDimmension(x1) + data.getDimmension(x1Offset); // Same x if vertical
		}
	}else{
		return data.getDimmension(x2) + data.getDimmension(x2Offset);
	}
}

double ReferenceLine::getEndY(LoadedData &data){
	if(oriented){
		if(types::getType(types::orientations, orientation) == types::Orientation::Vertical){
			return data.getDimmension(y1) + data.getDimmension(length) + data.getDimmension(y1Offset);
		}else{
			return data.getDimmension(y1) + data.getDimmension(y1Offset); // Same x if horizontal
		}
	}else{
		return data.getDimmension(y2) + data.getDimmension(y2Offset);
	}
}

////////////////////////////////////////////////////
/// Waypoint
////////////////////////////////////////////////////
double datafile::Waypoint::getX(LoadedData &data){
	if(reflineWaypoint){
		ReferenceLine rline = data.reflines[reflineName];
		double distanceAlong = data.getDimmension(distance) + data.getDimmension(distanceOffset);
		double distanceAbove = data.getDimmension(reflineOffset);
		Vector2 line(rline.getEndX(data) - rline.getStartX(data), rline.getEndY(data) - rline.getStartY(data));
		line.normalize(); // Unit vector
		Vector2 transform1 = line * distanceAlong; // Vector from point 1 to a point a distance along the line
		line.rotate(Vector2::pi / 2); // Rotate unit vector to be perpendicular (for distance away from line)
		Vector2 transform2 = line * distanceAbove; // Vector from point 1 to a point a distance above the line
		return rline.getStartX(data) + transform1.x + transform2.x;
	}else{
		return data.getDimmension(x) + data.getDimmension(xOffset);
	}
}

double datafile::Waypoint::getY(LoadedData &data){
	if(reflineWaypoint){
		ReferenceLine rline = data.reflines[reflineName];
		double distanceAlong = data.getDimmension(distance) + data.getDimmension(distanceOffset);
		double distanceAbove = data.getDimmension(reflineOffset);
		Vector2 line(rline.getEndX(data) - rline.getStartX(data), rline.getEndY(data) - rline.getStartY(data));
		line.normalize(); // Unit vector
		Vector2 transform1 = line * distanceAlong; // Vector from point 1 to a point a distance along the line
		line.rotate(Vector2::pi / 2); // Rotate unit vector to be perpendicular (for distance away from line)
		Vector2 transform2 = line * distanceAbove; // Vector from point 1 to a point a distance above the line
		return rline.getStartY(data) + transform1.y + transform2.y;
	}else{
		return data.getDimmension(y) + data.getDimmension(yOffset);
	}
}

double datafile::Waypoint::getHeading(LoadedData &data) {
	if (matchHeading) {
		ReferenceLine rline = data.reflines[reflineName];
		Vector2 line(rline.getEndX(data) - rline.getStartX(data), rline.getEndY(data) - rline.getStartY(data));
		line.rotate(Vector2::pi / 2);
		double heading = atan(line.y / line.x);
		if (!positiveMatch)
			heading -= Vector2::pi;
		return r2d(heading) + data.getAngle(headingOffset);
	} else {
		return data.getAngle(heading) + data.getAngle(headingOffset);
	}
	
}

////////////////////////////////////////////////////
/// LoadedData
////////////////////////////////////////////////////
double LoadedData::getDimmension(Dimmension d){
	if(d.measurement){
		if(measurements.find(d.measurementName) == measurements.end())
			return -1;
		return measurements[d.measurementName];
	}else{
		return d.value;
	}
}

double LoadedData::getAngle(Angle a){
	if(a.namedAngle){
		if(angles.find(a.angleName) == angles.end())
			return -1;
		return angles[a.angleName];
	}else{
		return a.value;
	}
}

void LoadedData::verifyData(){
	// Make sure unit is a known value
	types::getType(types::conversionFactors, unitName);
	// Make sure fitmethod is known
	types::getType(types::fitmethods, settings.fitmethod);
	// Make sure samplerate is known
	types::getType(types::samplerates, settings.samplerate);
	// Make sure drive train is known (and if swerve make sure wheelbaseDepth is specified)
	types::DriveTrain driveTrain = types::getType(types::drivetrains, settings.drivetrain);
	if(driveTrain == types::DriveTrain::Swerve && settings.wheelbaseDepth == -1)
		throw ParseException("'wheelbasedepth' must be specified for a swerve drive train.");
	// Make sure orientations are known for all oriented reference lines
	// Make sure all named dimmensions and angles have a value
	for(auto &it : reflines){
		if(it.second.oriented)
			types::getType(types::orientations, it.second.orientation);
		// will only have this if it is an oriented refline
		if(it.second.oriented && it.second.length.measurement && measurements.find(it.second.length.measurementName) == measurements.end())
			throw ParseException("No measurement with name '" + it.second.length.measurementName + "'.");
		if(it.second.x1.measurement && measurements.find(it.second.x1.measurementName) == measurements.end())
			throw ParseException("No measurement with name '" + it.second.x1.measurementName + "'.");
		if(it.second.y1.measurement && measurements.find(it.second.y1.measurementName) == measurements.end())
			throw ParseException("No measurement with name '" + it.second.y1.measurementName + "'.");
		// Will only have these if it is not an oriented refline
		if(!it.second.oriented && it.second.x2.measurement && measurements.find(it.second.x2.measurementName) == measurements.end())
			throw ParseException("No measurement with name '" + it.second.x2.measurementName + "'.");
		if(!it.second.oriented && it.second.y2.measurement && measurements.find(it.second.y2.measurementName) == measurements.end())
			throw ParseException("No measurement with name '" + it.second.y2.measurementName + "'.");
	}
	// Make sure all waypoint dimmensions/angles have a value and all reference line names are valid
	for(auto &it : paths){
		// Verify override settings
		if(it.second.overrideSettings.fitmethod != "")
			types::getType(types::fitmethods, it.second.overrideSettings.fitmethod);
		if(it.second.overrideSettings.samplerate != "")
			types::getType(types::samplerates, it.second.overrideSettings.samplerate);
		if(it.second.overrideSettings.drivetrain != ""){
			types::DriveTrain driveTrain = types::getType(types::drivetrains, it.second.overrideSettings.drivetrain);
			if(driveTrain == types::DriveTrain::Swerve && it.second.overrideSettings.wheelbaseDepth == -1)
				throw ParseException("'wheelbasedepth' must be specified for a swerve drive train.");
		}
		// Verify all waypoints
		for(auto &w : it.second.waypoints){
			// All waypoints
			if(w.heading.namedAngle && angles.find(w.heading.angleName) == angles.end())
				throw ParseException("No angle with name '" + w.heading.angleName + "'.");
			// Only if refline waypoint
			if(w.reflineWaypoint && reflines.find(w.reflineName) == reflines.end())
				throw ParseException("No reference line named '" + w.reflineName + "'.");
			if(w.reflineWaypoint && w.distance.measurement && measurements.find(w.distance.measurementName) == measurements.end())
				throw ParseException("No measurement with name '" + w.distance.measurementName + "'.");
			// Only if not refline waypoint
			if(!w.reflineWaypoint && w.x.measurement && measurements.find(w.x.measurementName) == measurements.end())
				throw ParseException("No measurement with name '" + w.x.measurementName + "'.");
			if(!w.reflineWaypoint && w.y.measurement && measurements.find(w.y.measurementName) == measurements.end())
				throw ParseException("No measurement with name '" + w.y.measurementName + "'.");
		}
	}
}

LoadedData LoadedData::convertToMeters() {
	// Get conversion factor
	double conversionFactor = types::getType(types::conversionFactors, unitName);

	// Construct converted LoadedData object without modifying current one
	LoadedData rtnData;
	rtnData.isInMeters = true;
	rtnData.width = width * conversionFactor;
	rtnData.height = height * conversionFactor;
	rtnData.unitName = unitName;

	// Convert pathfinder settings
	rtnData.settings = settings;
	rtnData.settings.velocity *= conversionFactor;
	rtnData.settings.acceleration *= conversionFactor;
	rtnData.settings.jerk *= conversionFactor;
	rtnData.settings.wheelbaseWidth *= conversionFactor;
	rtnData.settings.wheelbaseDepth *= conversionFactor;

	// Convert measurements
	rtnData.measurements = measurements;
	for (auto &it : rtnData.measurements) {
		rtnData.measurements[it.first] = it.second * conversionFactor;
	}

	rtnData.angles = angles; // Angles are not affected

	// Adjust dimmensions of reference lines
	rtnData.reflines = reflines;
	for (auto &it : rtnData.reflines) {
		if (!it.second.x1.measurement)
			it.second.x1.value *= conversionFactor;
		if (!it.second.y1.measurement)
			it.second.y1.value *= conversionFactor;
		if (!it.second.x2.measurement)
			it.second.x2.value *= conversionFactor;
		if (!it.second.y2.measurement)
			it.second.y2.value *= conversionFactor;
		if (!it.second.length.measurement)
			it.second.length.value *= conversionFactor;
		if(!it.second.x1Offset.measurement)
			it.second.x1Offset.value *= conversionFactor;
		if(!it.second.y1Offset.measurement)
			it.second.y1Offset.value *= conversionFactor;
		if(!it.second.x2Offset.measurement)
			it.second.x2Offset.value *= conversionFactor;
		if(!it.second.y2Offset.measurement)
			it.second.y2Offset.value *= conversionFactor;
	}

	// Adjust path waypoints
	rtnData.paths = paths;
	rtnData.pathNames = pathNames;
	for (auto &it : rtnData.paths) {
		// Convert override settings
		if (it.second.overrideSettings.velocity > -1)
			it.second.overrideSettings.velocity *= conversionFactor;
		if (it.second.overrideSettings.acceleration > -1)
			it.second.overrideSettings.acceleration *= conversionFactor;
		if (it.second.overrideSettings.jerk > -1)
			it.second.overrideSettings.jerk *= conversionFactor;
		if (it.second.overrideSettings.wheelbaseWidth > -1)
			it.second.overrideSettings.wheelbaseWidth *= conversionFactor;
		if (it.second.overrideSettings.wheelbaseDepth > -1)
			it.second.overrideSettings.wheelbaseDepth *= conversionFactor;
		for (auto &w : it.second.waypoints) {
			if (!w.distance.measurement)
				w.distance.value *= conversionFactor;
			if (!w.x.measurement)
				w.x.value *= conversionFactor;
			if (!w.y.measurement)
				w.y.value *= conversionFactor;
			if (!w.xOffset.measurement)
				w.xOffset.value *= conversionFactor;
			if (!w.yOffset.measurement)
				w.yOffset.value *= conversionFactor;
			if (!w.reflineOffset.measurement)
				w.reflineOffset.value *= conversionFactor;
			if(!w.distanceOffset.measurement)
				w.distanceOffset.value *= conversionFactor;
		}
	}

	return rtnData;
}

LoadedData LoadedData::convertToSpecifiedUnit() {
	throw "Convert to specified unit not implemented.";
}


////////////////////////////////////////////////////
/// Datafile
////////////////////////////////////////////////////
Datafile::Datafile(std::string filepath) : filepath(filepath) {  }

YAMLDatafile::YAMLDatafile(std::string filepath) : Datafile(filepath) {  }

void YAMLDatafile::parse(LoadedData &data) {
	YAML::Node yaml = YAML::LoadFile(filepath);
	LoadedData rawData = yaml.as<LoadedData>();
	rawData.verifyData();
	data = rawData.convertToMeters();
}

void YAMLDatafile::save(LoadedData data) {
	
}
