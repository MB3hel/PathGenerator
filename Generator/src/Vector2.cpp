#include "Vector2.hpp"

Vector2::Vector2(double x, double y) : x(x), y(y){}

// math operators

Vector2 Vector2::operator+ (double value){
    return Vector2(x + value, y + value);
}

Vector2 Vector2::operator+ (Vector2 &other){
    return Vector2(x + other.x, y + other.y);
}

Vector2 Vector2::operator- (double value){
    return Vector2(x - value, y - value);
}

Vector2 Vector2::operator- (Vector2 &other){
    return Vector2(x - other.x, y - other.y);
}

Vector2 Vector2::operator* (double value){
    return Vector2(x * value, y * value);
}

Vector2 Vector2::operator* (Vector2 &other){
    return Vector2(x * other.x, y * other.y);
}

Vector2 Vector2::operator/ (double value){
    return Vector2(x / value, y / value);
}

Vector2 Vector2::operator/ (Vector2 &other){
    return Vector2(x / other.x, y / other.y);
}

bool Vector2::operator== (Vector2 &other){
    return x == other.x && y == other.y;
}

bool Vector2::operator!= (Vector2 &other){
    return x != other.x || y != other.y;
}

double Vector2::cross(Vector2 &other){
	return (x * other.y) - (y * other.x);
}

double Vector2::dot(Vector2 &other){
    return (x * other.x) + (y * other.y);
}

void Vector2::scale(double factor){
    x *= factor;
    y *= factor;
}

void Vector2::normalize(){
    x /= length();
    y /= length();
}

Vector2 Vector2::min(Vector2 &a, Vector2 &b){
    return Vector2(std::min(a.x, b.x), std::min(a.y, b.y));
}

Vector2 Vector2::max(Vector2 &a, Vector2 &b){
    return Vector2(std::max(a.x, b.x), std::max(a.y, b.y));
}

Vector2 Vector2::clamp(Vector2 &value, Vector2 &min, Vector2 &max){
  return Vector2(Vector2::clamp(value.x, min.x, max.x), Vector2::clamp(value.y, min.y, max.y));
}

Vector2 Vector2::lerp(Vector2 &a, Vector2 &b, double amount){
    amount = Vector2::clamp(amount, 0, 1);
    return Vector2(a.x + (b.x - a.x) * amount, a.y + (b.y - a.y) * amount);
}

Vector2 Vector2::rotate(Vector2 &vec, double angle){
  angle = (2 * pi) - angle;
  return Vector2((vec.x * cos(angle)) + (vec.y * -sin(angle)), (vec.x * sin(angle)) + (vec.y * cos(angle))); 
}

void Vector2::rotate(double angle){
    angle = (2 * pi) - angle;
    double xn = (x * cos(angle)) + (y * -sin(angle));
    double yn = (x * sin(angle)) + (y * cos(angle));
    x = xn;
    y = yn;
}


void Vector2::rotateAboutTarget(Vector2 &target, double angle){
    Vector2 temp;
    temp.x = x - target.x;
    temp.y = y - target.y;
    temp = rotate(temp, angle);
    x = target.x + temp.x;
    y = target.y + temp.y;
    
}

double Vector2::length(){
    return sqrt((x * x) + (y * y));
}

void Vector2::negate(){
    x *= -1;
    y *= -1;
}

std::string Vector2::str(){
    std::stringstream buf;
    buf << "(" << x << ", " << y << ")";
    return buf.str();
}

double Vector2::clamp(double v, double bound1, double bound2){
  double lower = std::min(bound1, bound2);
  double upper = std::max(bound1, bound2);
  return std::max(lower, std::min(v, upper));
}
