#include <DatafileGUI.hpp>

RefLineView::RefLineView(wxWindow *parent) : RefLineViewBase(parent) {
	updateShownItems();
}

void RefLineView::typeSelected(wxCommandEvent &event) {
	updateShownItems();
}

void RefLineView::updateShownItems() {
	switch (refLineType->GetCurrentSelection()) {
	case HORIZ:
	case VERT:
		x1Label->SetLabel("x");
		y1Label->SetLabel("y");

		x2Label->Hide();
		y2Label->Hide();
		x2Value->Hide();
		y2Value->Hide();
		lengthLabel->Show();
		lengthValue->Show();
		break;
	case CUSTOM:
		x1Label->SetLabel("x1");
		y1Label->SetLabel("y1");
		
		x2Label->Show();
		y2Label->Show();
		x2Value->Show();
		y2Value->Show();
		lengthLabel->Hide();
		lengthValue->Hide();
		break;
	}

	this->Layout();
}

DatafileGUI::DatafileGUI(wxWindow *parent) : DatafileGUIBase(parent) {
	RefLineView *rl1 = new RefLineView(mainContent);
	scrollContentSizer->Add(rl1, 0);
	rl1->FitInside();
}