#include "AboutDialog.hpp"

AboutDialog::AboutDialog(wxWindow *parent) : AboutDialogBase(parent) {
	this->LicenseTextView->SetValue(fixText(wxString::FromUTF8((const char*)license_txt, license_txt_size)));
	this->pathfinderView->SetValue(fixText(wxString::FromUTF8((const char*)pathfinder_license_txt, pathfinder_license_txt_size)));
	this->wxwReadmeView->SetValue(fixText(wxString::FromUTF8((const char*)wxwidgetslicense_txt, wxwidgetslicense_txt_size)));
	this->versionLabel->SetLabel(fixText(wxString::FromUTF8((const char*)version_txt, version_txt_size)));
	this->yamlLicneseView->SetValue(fixText(wxString::FromUTF8((const char*)yamllicense_txt, yamllicense_txt_size)));
    
    
}

wxString AboutDialog::fixText(wxString toFix){
    toFix.Replace("\r\n", "\n"); // Ensure only one newline character
    return toFix;
}
