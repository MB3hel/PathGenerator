#if defined _WIN32 || defined _WIN64
#define WIN32_LEAN_AND_MEAN
#endif

#include <app.hpp>

// XPM Images (These are used on non-windows platforms. Win resources (win_res.rc) are used on windows.
#ifndef wxHAS_IMAGES_IN_RESOURCES
#include "iconsmall.xpm"
#endif

///////////////////////////////////////////////////
/// PathGeneratorApp (main app)
///////////////////////////////////////////////////

PreviewWindow *PathGenerator::previewWindow = nullptr;
DatafileGUI *PathGenerator::datafileWindow = nullptr;

IMPLEMENT_APP(PathGenerator);

bool PathGenerator::OnInit() {
	if(previewWindow != nullptr)
		return false;
	if (!wxApp::OnInit())
		return false;
#if (defined _WIN32 || defined _WIN64) /*&& defined _DEBUG*/
	// Open a console window to show cout and cerr messages on windows MSVC (Visual Studio does not show these)
	if (AllocConsole()) {
		FILE *pNewStdout = nullptr;
		FILE *pNewStderr = nullptr;
		FILE *pNewStdin = nullptr;
		freopen_s(&pNewStdout, "CONOUT$", "w", stdout);
		freopen_s(&pNewStderr, "CONOUT$", "w", stderr);
		freopen_s(&pNewStdin, "CONIN$", "r", stdin);
	}

#endif

	// Make sure all image handlers are initialized before trying to load images
	wxInitAllImageHandlers();

	previewWindow = new PreviewWindow(NULL);
	previewWindow->SetIcon(wxICON(iconsmall));
	previewWindow->Show();

	//datafileWindow = new DatafileGUI(0);
	//datafileWindow->SetIcon(wxICON(icon));
	//datafileWindow->Show();

	return true;
}

int PathGenerator::OnExit() {
	return 0;
}
