#include <PathData.hpp>

//////////////////////////////////////////////////////////////////////////
/// PathData
//////////////////////////////////////////////////////////////////////////

PathData PathData::generatePath(std::string name, datafile::Path path, datafile::LoadedData &data, bool fast){
    PathData pathData;
    pathData.path = path;
    pathData.name = name;
    
    // Create array of pathfinder waypoints
    pathData.waypoints = new Waypoint[pathData.path.waypoints.size()]();
	pathData.waypointCount = pathData.path.waypoints.size();
    for(size_t i = 0; i < pathData.path.waypoints.size(); ++i){
		datafile::Waypoint pt = pathData.path.waypoints[i];
		Waypoint pathfinderPt = { pt.getX(data), pt.getY(data), d2r(pt.getHeading(data)) };
        pathData.waypoints[i] = pathfinderPt;
    }

    int result = 0;

	int samples;
    if(!fast){
        if(path.overrideSettings.samplerate == "")
            samples = types::getType(types::samplerates, data.settings.samplerate);
        else
            samples = types::getType(types::samplerates, path.overrideSettings.samplerate);
    }else {
        samples = PATHFINDER_SAMPLES_FAST;
    }

    types::FitFunction fm;
    if(path.overrideSettings.fitmethod == "")
        fm = types::getType(types::fitmethods, data.settings.fitmethod);
    else
        fm = types::getType(types::fitmethods, path.overrideSettings.fitmethod);

    // Generate base trajectory
    pathData.candidate = new TrajectoryCandidate();
    result = pathfinder_prepare(pathData.waypoints, 
        pathData.path.waypoints.size(), 
        fm,
        samples, 
		path.overrideSettings.timestep == -1 ? data.settings.timestep : path.overrideSettings.timestep, 
		path.overrideSettings.velocity == -1 ? data.settings.velocity : path.overrideSettings.velocity, 
		path.overrideSettings.acceleration == -1 ? data.settings.acceleration : path.overrideSettings.acceleration, 
		path.overrideSettings.jerk == -1 ? data.settings.jerk : path.overrideSettings.jerk, 
		pathData.candidate);
    if(result < 0){
        pathData.errorCode = result;
        pathData.errorStage = "prepare";
        return pathData;
    }
    pathData.trajectoryLength = pathData.candidate->length;
    pathData.baseTrajectory = new Segment[pathData.trajectoryLength]();
    result = pathfinder_generate(pathData.candidate, pathData.baseTrajectory);
    if(result < 0){
        pathData.errorCode = result;
        pathData.errorStage = "generate";
        return pathData;
    }

    // Modify for the correct drive train
    types::DriveTrain dt;
    if(path.overrideSettings.drivetrain == "")
        dt = types::getType(types::drivetrains, data.settings.drivetrain);
    else
        dt = types::getType(types::drivetrains, path.overrideSettings.drivetrain);
    
    if(dt == types::DriveTrain::Tank){
        pathData.frontLeftTrajectory = new Segment[pathData.trajectoryLength]();
        pathData.frontRightTrajectory = new Segment[pathData.trajectoryLength]();
        pathfinder_modify_tank(pathData.baseTrajectory, 
            pathData.trajectoryLength, 
            pathData.frontLeftTrajectory, 
            pathData.frontRightTrajectory, 
			path.overrideSettings.wheelbaseWidth == -1 ? data.settings.wheelbaseWidth : path.overrideSettings.wheelbaseWidth);
    }else{
        pathData.frontLeftTrajectory = new Segment[pathData.trajectoryLength]();
        pathData.frontRightTrajectory = new Segment[pathData.trajectoryLength]();
        pathData.rearLeftTrajectory = new Segment[pathData.trajectoryLength]();
        pathData.rearRightTrajectory = new Segment[pathData.trajectoryLength]();
        pathfinder_modify_swerve(pathData.baseTrajectory, 
            pathData.trajectoryLength, 
            pathData.frontLeftTrajectory, 
            pathData.frontRightTrajectory, 
            pathData.rearLeftTrajectory, 
            pathData.rearRightTrajectory, 
			path.overrideSettings.wheelbaseWidth == -1 ? data.settings.wheelbaseWidth : path.overrideSettings.wheelbaseWidth,
			path.overrideSettings.wheelbaseDepth == -1 ? data.settings.wheelbaseDepth : path.overrideSettings.wheelbaseDepth,
            SWERVE_DEFAULT);
    }

    return pathData;
}

bool PathData::savePath(std::string outputPath, datafile::LoadedData &data){
    // Create a duplicate PathData object that generated a path using the correct sample rate
    PathData dupData = PathData::generatePath(name, path, data, false);
    
    types::DriveTrain dt;
    if(path.overrideSettings.drivetrain == "")
        dt = types::getType(types::drivetrains, data.settings.drivetrain);
    else
        dt = types::getType(types::drivetrains, path.overrideSettings.drivetrain);

    // If tank save the left and right configs else save all four trajectories
    if(dt == types::DriveTrain::Tank){
        FILE *lFile = fopen((outputPath + "/" + name + "_left.csv").c_str(), "w");
        if(!lFile)
            return false;
        pathfinder_serialize_csv(lFile, dupData.frontLeftTrajectory, dupData.trajectoryLength);
        fclose(lFile);

        FILE *rFile = fopen((outputPath + "/" + name + "_right.csv").c_str(), "w");
        if(!rFile)
            return false;
        pathfinder_serialize_csv(rFile, dupData.frontRightTrajectory, dupData.trajectoryLength);
        fclose(rFile);
    }else{
        FILE *flFile = fopen((outputPath + "/" + name + "_frontleft.csv").c_str(), "w");
        if(!flFile)
            return false;
        pathfinder_serialize_csv(flFile, dupData.frontLeftTrajectory, dupData.trajectoryLength);
        fclose(flFile);

        FILE *frFile = fopen((outputPath + "/" + name + "_frontright.csv").c_str(), "w");
        if(!frFile)
            return false;
        pathfinder_serialize_csv(frFile, dupData.frontRightTrajectory, dupData.trajectoryLength);
        fclose(frFile);

        FILE *rlFile = fopen((outputPath + "/" + name + "_rearleft.csv").c_str(), "w");
        if(!rlFile)
            return false;
        pathfinder_serialize_csv(rlFile, dupData.rearLeftTrajectory, dupData.trajectoryLength);
        fclose(rlFile);

        FILE *rrFile = fopen((outputPath + "/" + name + "_rearright.csv").c_str(), "w");
        if(!rrFile)
            return false;
        pathfinder_serialize_csv(rrFile, dupData.rearRightTrajectory, dupData.trajectoryLength);
        fclose(rrFile);
    }

    return true;
}