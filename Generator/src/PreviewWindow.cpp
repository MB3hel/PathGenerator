
#include <PreviewWindow.hpp>

#include <wx/dcclient.h>
#include <wx/filedlg.h>
#include <wx/dcgraph.h>
#include <wx/dirdlg.h>
#include <wx/msgdlg.h>
#include <wx/time.h> 
#include <wx/app.h>
#include <wx/dir.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <future>
#include <functional>
#include <chrono>
#include <thread>
#include <memory>

//////////////////////////////////////////////////////////////////////////
/// PreviewWindow
//////////////////////////////////////////////////////////////////////////

PreviewWindow::PreviewWindow(wxWindow *parent) : PreviewWindowBase(parent){
    // On Windows Checkboxes work in list mode. On other OSes checkboxes only work with report mode
	pathList->InsertColumn(0, wxT(""), 0, pathList->GetSize().x - 1 - wxSystemSettings::GetMetric(wxSYS_VSCROLL_X));
	// This event is not yet known to wxFormBuilder so bind it here
	pathList->Connect(wxEVT_LIST_ITEM_CHECKED, wxListEventHandler(PreviewWindow::pathCheckChanged), NULL, this);
	pathList->Connect(wxEVT_LIST_ITEM_UNCHECKED, wxListEventHandler(PreviewWindow::pathCheckChanged), NULL, this);
	pathList->EnableCheckBoxes();
}

PreviewWindow::~PreviewWindow(){
    if(generatePathThread != nullptr){
        generatePathThread->join();
        delete generatePathThread;
    }
    if(savePathThread != nullptr){
        savePathThread->join();
        delete savePathThread;
    }
}

double PreviewWindow::getXPixels(double xMeters){
    return (xMeters / data.width) * xPixels + xOffset;
}

double PreviewWindow::getYPixels(double yMeters){
    // Pathfinder uses bottom left as (0,0) like normal coord plane. Panel uses top left as (0,0)
    // Everything uses the pathfinder coordinate space (bottom left is 0,0) until here
    // Need to flip y coord along horizontal midline
    double panelYMeters = abs(yMeters - data.height); // This is converted into panel space not pathfinder space
    return (panelYMeters / data.height) * yPixels + yOffset; // Convert meters in panel space into pixels in panel space
}

void PreviewWindow::refreshDatafile( wxCommandEvent& event ) {
    if(datafilePath == ""){
        std::cerr << "No datafile selected" << std::endl;
        return;
    }
    dataUnloaded();
	YAMLDatafile datafile(datafilePath);

    try{
	    datafile.parse(data);
        std::cout << "Parsed data successfully." << std::endl;
        mnuRefresh->Enable();
        rootPanel->Refresh();
        dataLoaded();
    }catch(std::exception &e){
        std::cerr << "Failed to parse data file with exception:" << std::endl << e.what() << std::endl;
    }
}

void PreviewWindow::loadDataFile( wxCommandEvent& event ){
    wxFileDialog openFileDialog(this, _("Open Config File"), "", "",
                    "YAML files (*.yaml)|*.yaml", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
    if (openFileDialog.ShowModal() == wxID_CANCEL)
        return;
    dataUnloaded();
    datafilePath = openFileDialog.GetPath().ToStdString();
	YAMLDatafile datafile(datafilePath);

    try{
	    datafile.parse(data);
        std::cout << "Parsed data successfully." << std::endl;
        mnuRefresh->Enable();
        rootPanel->Refresh();
        dataLoaded();
    }catch(std::exception &e){
        std::cerr << "Failed to parse data file with exception:" << std::endl << e.what() << std::endl;
    }
}

void PreviewWindow::dataLoaded(){
    hasLoadedData = true;
    // Create a PathData object for each loaded path
    // Should start generation for each path asyncrenously and then spawn a thread that will wait on all the futures
    // The thread should be joined on exit and before starting another batch of async methods

    // Wait for current generate thread to quit
    if(generatePathThread != nullptr){
        generatePathThread->join();
        delete generatePathThread;
        generateFutures.clear();
    }

    pathDataMutex.lock();
    pathDatas.clear();
    pathDataMutex.unlock();

    hasGeneratedPathData = false;
    
    // Start each generation as an async task
    for(size_t i = 0; i < data.pathNames.size(); ++i){
        std::string &name = data.pathNames[i];
        datafile::Path &path = data.paths[name];
        std::future<PathData> f = std::async(std::launch::async, [&]()->PathData{
            return PathData::generatePath(name, path, data, true);
        });
        generateFutures.push_back(std::move(f)); // Futures cannot be copied. They must be moved
    }

    // Spawn the new generate thread
    generatePathThread = new std::thread(&PreviewWindow::handlePathGeneration, this);

    // Draw everything but paths now
    rootPanel->Refresh();
}

void PreviewWindow::pathCheckChanged(wxListEvent &event) {
	// Redraw paths
	rootPanel->Refresh();
}

void PreviewWindow::handlePathGeneration(){
    pathDataMutex.lock();
    std::vector<std::string> pathNames;
    // This will block until each one is done
    for(auto &it : generateFutures){
        PathData pdata = it.get();
        if(pdata.errorCode == 0){
            pathDatas.push_back(pdata);
            pathNames.push_back(pdata.name);
        }
        else{
            std::cerr << "Error " << pdata.errorCode << " while generating path in " << pdata.errorStage << " stage." << std::endl;
        }
    }
    std::cout << "Done generating all paths." << std::endl;

    pathDataMutex.unlock();

    // Add each path to the UI list
    wxEvtHandler::CallAfter([&,pathNames](){
        pathDataMutex.lock();
        for(auto &name : pathNames){
            pathList->InsertItem(pathList->GetItemCount(), name);
            pathList->CheckItem(pathList->GetItemCount() - 1, true);
        }
        hasGeneratedPathData = true; // Do not allow redraw until all paths are in the list
        pathDataMutex.unlock();
        rootPanel->Refresh();
    });
}

void PreviewWindow::dataUnloaded(){
    // Clear list of paths (UI)
    // Cannot use ClearAll because that clears columns too
    int count = pathList->GetItemCount();
    for(size_t i = 0; i < count; ++i){
        pathList->DeleteItem(0);
    }

    hasLoadedData = false;
    hasGeneratedPathData = false;
    rootPanel->Refresh();
}

void PreviewWindow::drawnItemsChanged(wxCommandEvent &event){
    rootPanel->Refresh();
}

void PreviewWindow::aboutSelected(wxCommandEvent &event) {
	aboutDialog.Show();
}

void PreviewWindow::savePaths(wxCommandEvent &event) {

    if(!hasLoadedData){
        wxMessageDialog messageDialog(this, "Cannot Save", "", wxOK | wxCENTER | wxICON_WARNING);
        messageDialog.SetExtendedMessage("No paths to save. Open a datafile first.");
        messageDialog.ShowModal();
        return;
    }
    
    wxDirDialog openDialog(this, "Choose Path Output Directory", "",
            wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST);

    if (openDialog.ShowModal() == wxID_CANCEL){
        pathDataMutex.unlock();
        return;
    }

    std::string outputPath = openDialog.GetPath().ToStdString();

	wxDir directory(outputPath);

	if (!directory.IsOpened()) {
		std::cerr << "Could not open chosen directory. Make sure it exists and you have permission to access it." << std::endl;
		return;
	}

	wxString fileName;
	directory.GetFirst(&fileName);
	if (fileName != wxEmptyString) {
		wxMessageDialog warningDialog(this, "Files Exist", "", 
			wxYES_NO | wxNO_DEFAULT | wxCENTER | wxICON_WARNING);
		warningDialog.SetExtendedMessage("The chosen directory already contains other files. Some may be overriden. Continue anyways?");
		if (warningDialog.ShowModal() == wxID_NO) {
			std::cerr << "Save canceled by user." << std::endl;
			return;
		}
	}

    progressDialog = new wxProgressDialog("Saving Paths", "Saving paths...", pathDatas.size() + 1, this);

    if(savePathThread != nullptr){
        savePathThread->join();
        delete savePathThread;
    }

    isSavingPaths = true;

    savePathThread = new std::thread(&PreviewWindow::handlePathSave, this, outputPath);
}

void PreviewWindow::handlePathSave(std::string outputPath){
    pathDataMutex.lock();

    int error = -1;
    bool success = true;
    for(size_t i = 0; i < pathDatas.size(); ++i){
        wxEvtHandler::CallAfter([&](){
            if(progressDialog != nullptr)
                progressDialog->Update(i + 1, "Saving '" + pathDatas[i].name + "'...");
        });
        std::future<bool> fut = std::async(std::launch::async, [&]()->bool{
            return pathDatas[i].savePath(outputPath, data);
        });
        while(fut.wait_for(std::chrono::milliseconds(100)) == std::future_status::timeout) { }
        if(!fut.get()){
            error = i;
            break;
        }
    }

    wxEvtHandler::CallAfter([=](){
        progressDialog->Update(pathDatas.size() + 1, "Done");
        progressDialog->Destroy();
        wxYield();
        if(success){
            wxMessageDialog messageDialog(this, "Paths Saved Successfully", "", wxOK | wxCENTER);
            messageDialog.SetExtendedMessage("All paths were successfully saved to '" + outputPath + "'.");
            messageDialog.ShowModal();
        }else{
            wxMessageDialog messageDialog(this, "Error Saving Paths", "", wxOK | wxCENTER | wxICON_ERROR);
            messageDialog.SetExtendedMessage("Error saving path '" + pathDatas[error].name + "'. Make sure the output folder exists and is writable.");
            messageDialog.ShowModal();
        }
    });

    pathDataMutex.unlock();
    isSavingPaths = false;
}

void PreviewWindow::onPaint(wxPaintEvent &event){

    if(!hasLoadedData || isSavingPaths)
        return;

    wxPaintDC pdc(rootPanel);

#if wxUSE_GRAPHICS_CONTEXT
    // Use GDI+ if available (enables AA for pens)
    wxGCDC gdc( pdc ) ;
    wxDC &dc = (wxDC&) gdc;
#else
    wxDC &dc = pdc ;
#endif
    
    // Calculate dimmensions of the field rectangle
    double inchRatio = data.width / data.height;
    double pxRatio = rootPanel->GetSize().GetWidth() / rootPanel->GetSize().GetHeight();
    double rectHeight = 0, rectWidth = 0;
    if(pxRatio >= inchRatio){
        // Height restricted
        yPixels = rootPanel->GetSize().GetHeight() - MIN_BORDER;
        xPixels = inchRatio * yPixels;
        xOffset = (rootPanel->GetSize().GetWidth() - xPixels) / 2;
        yOffset = MIN_BORDER / 2;
    }else{
        // Width restricted
        xPixels = rootPanel->GetSize().GetWidth() - MIN_BORDER;
        yPixels = (1 / inchRatio) * xPixels;
        yOffset = (rootPanel->GetSize().GetHeight() - yPixels) / 2;
        xOffset = MIN_BORDER / 2;
    }

    // Draw field perimeter and midlines
    dc.SetPen(*wxBLACK_PEN);
    if(mnuShowBorders->IsChecked()){
        dc.DrawRectangle(wxPoint(xOffset, yOffset),  wxSize(xPixels, yPixels));
    }
    dc.SetPen(*wxGREY_PEN);
    if(mnuShowMidlines->IsChecked()){
        dc.DrawLine(wxPoint(xOffset, (yPixels / 2) + yOffset), wxPoint(xPixels + xOffset, (yPixels / 2) + yOffset));
        dc.DrawLine(wxPoint((xPixels / 2) + xOffset, yOffset), wxPoint((xPixels / 2) + xOffset, yPixels + yOffset));
    }

    // Draw each reference line
    if(mnuShowRefLines->IsChecked()){
        dc.SetPen(reflinePen);
        for (auto const& refLine : data.reflines){
            datafile::ReferenceLine rlne = refLine.second;
            double sx = getXPixels(rlne.getStartX(data));
            double sy = getYPixels(rlne.getStartY(data));
            double ex = getXPixels(rlne.getEndX(data));
            double ey = getYPixels(rlne.getEndY(data));
            dc.DrawLine(wxPoint(sx, sy), wxPoint(ex, ey));
        }
    }

    // Draw each point for each path (only once generated)
    // Can only draw once the list of paths is complete. This occurs after generation
    dc.SetPen(*wxGREEN_PEN);
	if(hasGeneratedPathData){
        int index = 0;
        for (auto const &name : data.pathNames){
            auto const &path = data.paths[name];
            if (pathList->IsItemChecked(index++)) {
                for (size_t j = 0; j < path.waypoints.size(); ++j) {
                    datafile::Waypoint waypoint = path.waypoints[j];
                    double x = getXPixels(waypoint.getX(data));
                    double y = getYPixels(waypoint.getY(data));
                    dc.DrawCircle(x, y, 2);
                }
            }
        }
    }

    // Draw the actual path based on the generated PathData
    dc.SetPen(*wxGREEN_PEN);
    if(hasGeneratedPathData){
        pathDataMutex.lock();
		for (size_t i = 0; i < pathDatas.size(); ++i) {
			if (pathList->IsItemChecked(i)) {
				auto &path = pathDatas[i];
				wxPoint *points = new wxPoint[path.trajectoryLength]();
				for (int i = 0; i < path.trajectoryLength; ++i) {
					points[i] = wxPoint(getXPixels(path.baseTrajectory[i].x), getYPixels(path.baseTrajectory[i].y));
				}
				dc.DrawSpline(path.trajectoryLength, points);
				delete points;
			}
        }
        pathDataMutex.unlock();
    }
}
