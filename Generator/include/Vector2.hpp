#pragma once 

#include <cmath>
#include <string>
#include <sstream>
#include <algorithm>

// Based on XNA Vector2

class Vector2{
public:
    static constexpr double pi = 3.14159265358979323846;
    double x, y;

    Vector2(double x = 0, double y = 0);

    Vector2 operator+ (double value);
    Vector2 operator+ (Vector2 &other);
    Vector2 operator- (double value);
    Vector2 operator- (Vector2 &other);
    Vector2 operator* (double value);
    Vector2 operator* (Vector2 &other);
    Vector2 operator/ (double value);
    Vector2 operator/ (Vector2 &other);
    bool operator== (Vector2 &other);
    bool operator!= (Vector2 &other);

	double cross(Vector2 &other);
    double dot(Vector2 &other);
    void scale(double factor);

    void normalize();

	static Vector2 clamp(Vector2 &value, Vector2 &min, Vector2 &max);
    static Vector2 min(Vector2 &a, Vector2 &b);
    static Vector2 max(Vector2 &a, Vector2 &b);
    

    static Vector2 lerp(Vector2 &a, Vector2 &b, double amount);

    static Vector2 rotate(Vector2 &vec, double angle);
  
    void rotate(double angle);
    void rotateAboutTarget(Vector2 &target, double angle);

    double length();
    void negate();
  
    std::string str();

private:
  static double clamp(double v, double bound1, double bound2);

};
