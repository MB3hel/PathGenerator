#pragma once

#include <PathGenratorUI.h>
#include <resources.h>

class AboutDialog : public AboutDialogBase {
public:
	AboutDialog(wxWindow *parent);

private:
    wxString fixText(wxString toFix);
};
