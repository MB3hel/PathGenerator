#pragma once

#include <PreviewWindow.hpp>
#include <DatafileGUI.hpp>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

/**
 * The main application
 */
class PathGenerator : public wxApp {
private:
	static PreviewWindow *previewWindow;
	static DatafileGUI *datafileWindow;
public:
	virtual bool OnInit() override;
	virtual int OnExit() override;
};

DECLARE_APP(PathGenerator);
