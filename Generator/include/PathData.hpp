#pragma once

#include <pathfinder.h>
#include <string>
#include <DataManager.hpp>

class PathData{
public:

    PathData(){}
    int errorCode = 0;
    std::string errorStage = "";

    datafile::Path path;
    std::string name = "";

    // Pathfinder objects/data
	int waypointCount;
    Waypoint *waypoints;
    TrajectoryCandidate *candidate;
    int trajectoryLength = 0;
    double wheelbaseWidth = 0, wheelbaseDepth = 0;
    // 4 trajectories in case swerve, but if tank only front ones are used
    Segment *baseTrajectory,
            *frontLeftTrajectory,
            *frontRightTrajectory,
            *rearLeftTrajectory,
            *rearRightTrajectory;
    
    /**
     * Generate the pathfinder path data from a path
     */
    static PathData generatePath(std::string name, datafile::Path path, datafile::LoadedData &data, bool fast = false);

    /**
     * Save the path to a file
     */
    bool savePath(std::string outputPath, datafile::LoadedData & data);
};