#pragma once

#include <PathGenratorUI.h>
#include <DataManager.hpp>
#include <AboutDialog.hpp>
#include <PathData.hpp>

#include <wx/progdlg.h>

#include <vector>
#include <functional>
#include <future>
#include <thread>
#include <mutex>
#include <memory>

#include <wx/pen.h>

#include <pathfinder.h>

#define MIN_BORDER 20

class PreviewWindow : public PreviewWindowBase{
public:
    PreviewWindow(wxWindow *parent);
    virtual ~PreviewWindow();

protected:
    void onPaint(wxPaintEvent& event) override;
    void loadDataFile(wxCommandEvent& event) override;
    void drawnItemsChanged(wxCommandEvent &event) override;
    void savePaths(wxCommandEvent &event) override;
	void aboutSelected(wxCommandEvent &event) override;
    void refreshDatafile( wxCommandEvent& event ) override;


private:

	AboutDialog aboutDialog{ this };

    std::string datafilePath = "";

    double xPixels = 0, yPixels = 0, xOffset = 0, yOffset = 0;
    datafile::LoadedData data;
    std::vector<PathData> pathDatas;
    bool hasLoadedData = false;
    bool hasGeneratedPathData = false;
    bool isSavingPaths = false;

    wxProgressDialog *progressDialog;
    std::vector<std::future<PathData>> generateFutures;
    std::thread *generatePathThread = nullptr, *savePathThread = nullptr;
    std::mutex pathDataMutex;

    wxPen reflinePen {wxColor(255, 153, 0, 255), 1};
    double getXPixels(double xMeters);
    double getYPixels(double yMeters);

    void dataLoaded();
    void dataUnloaded();

    void handlePathGeneration();
    void handlePathSave(std::string outputPath);

	void pathCheckChanged(wxListEvent &event);
};