#pragma once

#define KEY_UNIT "unit"
#define KEY_WIDTH "width"
#define KEY_HEIGHT "height"

#define KEY_PATHFINDERSETTINGS "pathfindersettings"
#define KEY_PATHFINDER_TIMESTEP "timestep"
#define KEY_PATHFINDER_VELOCITY "velocity"
#define KEY_PATHFINDER_ACCELERATION "acceleration"
#define KEY_PATHFINDER_JERK "jerk"
#define KEY_PATHFINDER_FITMETHOD "fitmethod"
#define KEY_PATHFINDER_SAMPLERATE "samplerate"
#define KEY_PATHFINDER_WHEELBASEWIDTH "wheelbasewidth"
#define KEY_PATHFINDER_WHEELBASEDEPTH "wheelbasedepth"
#define KEY_PATHFINDER_DRIVETRAIN "drivetrain"

#define KEY_MEASUREMENTS "measurements"

#define KEY_ANGLES "angles"

#define KEY_REFLINES "reflines"
#define KEY_REFLINE_X "x"
#define KEY_REFLINE_Y "y"
#define KEY_REFLINE_LENGTH "length"
#define KEY_REFLINE_ORIENTATION "orientation"
#define KEY_REFLINE_X1 "x1"
#define KEY_REFLINE_X2 "x2"
#define KEY_REFLINE_Y1 "y1"
#define KEY_REFLINE_Y2 "y2"
#define KEY_REFLINE_XOFFSET "xoffset"
#define KEY_REFLINE_YOFFSET "yoffset"
#define KEY_REFLINE_X1OFFSET "x1offset"
#define KEY_REFLINE_X2OFFSET "x2offset"
#define KEY_REFLINE_Y1OFFSET "y1offset"
#define KEY_REFLINE_Y2OFFSET "y2offset"

#define KEY_PATHS "paths"
#define KEY_PATH_OVERRIDESETTINGS "pathfindersettings"
#define KEY_PATH_WAYPOINTS "waypoints"

#define KEY_WAYPOINT_X "x"
#define KEY_WAYPOINT_Y "y"
#define KEY_WAYPOINT_XOFFSET "xoffset"
#define KEY_WAYPOINT_YOFFSET "yoffset"

#define KEY_WAYPOINT_HEADING "heading"
#define KEY_WAYPOINT_HEADING_OFFSET "headingoffset"

#define KEY_WAYPOINT_REFLINE "refline"
#define KEY_WAYPOINT_DISTANCE "distance"
#define KEY_WAYPOINT_DISTANCE_OFFSET "distanceoffset"
#define KEY_WAYPOINT_RLOFFSET "offset"
