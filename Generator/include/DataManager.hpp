#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <unordered_map>
#include <memory>
#include <mutex>

#include <Vector2.hpp>

#include <DatafileDefs.hpp>

#include <yaml-cpp/yaml.h>

#include <pathfinder.h>

class ParseException : public std::exception{
public:
	ParseException(std::string errorMessage);
	const char *what() const throw();

private:
	std::string errorMessage;
};

/**
 * Conversion from strings (datfile) to types needed by pathfinder/other objects
 */
namespace types {

class NoSuchKeyException : public std::exception {
public:
	NoSuchKeyException(std::string errorMessage);
	const char *what() const throw();

private:
	std::string errorMessage;
};

class NoSuchItemException : public std::exception{
public:
	NoSuchItemException(std::string errorMessage);
	const char *what() const throw();

private:
	std::string errorMessage;
};


typedef void(*FitFunction)(::Waypoint, ::Waypoint, ::Spline*);

enum class Orientation { None, Vertical, Horizontal };
enum class DriveTrain { None, Tank, Swerve };

const std::string UNIT_METERS = "meters";
const std::string UNIT_FEET = "feet";
const std::string UNIT_INCHES = "inches";

const std::unordered_map<std::string, double> conversionFactors = {
	{UNIT_METERS, 1},
	{UNIT_FEET, 0.3048},
	{UNIT_INCHES, 0.0254}
};

const std::unordered_map<std::string, FitFunction> fitmethods = {
	{"cubic", FIT_HERMITE_CUBIC},
	{"quintic", FIT_HERMITE_QUINTIC}
};

const std::unordered_map<std::string, int> samplerates = {
	{"fast", PATHFINDER_SAMPLES_FAST},
	{"low", PATHFINDER_SAMPLES_LOW},
	{"high", PATHFINDER_SAMPLES_HIGH}
};

const std::unordered_map<std::string, Orientation> orientations = {
	{"vertical", Orientation::Vertical},
	{"horizontal", Orientation::Horizontal}
};

const std::unordered_map<std::string, DriveTrain> drivetrains = {
	{"tank", DriveTrain::Tank},
	{"swerve", DriveTrain::Swerve}
};

template<class T>
T getType(std::unordered_map<std::string, T> map, std::string name) {
	if (map.find(name) == map.end())
		throw NoSuchKeyException("Unknown value for name '" + name + "'.");
	return map[name];
}

template<class T>
std::string getString(std::unordered_map<std::string, T> map, T object) {
	for (auto &it : map) {
		if (it.second == object) {
			return it.first;
		}
	}
	throw NoSuchItemException("Unknown item.");
}


} // namespace types

namespace datafile{


struct LoadedData; // Forward declare

struct PathfinderSettings {
	double timestep = -1;
	double velocity = -1;
	double acceleration = -1;
	double jerk = -1;
	std::string fitmethod = "";
	std::string samplerate = "";
	double wheelbaseWidth = -1;
	double wheelbaseDepth = -1;
	std::string drivetrain = "";

	bool operator==(const PathfinderSettings &obj) {
		return timestep == obj.timestep &&
			velocity == obj.velocity &&
			acceleration == obj.acceleration &&
			jerk == obj.jerk &&
			fitmethod == obj.fitmethod &&
			samplerate == obj.samplerate &&
			wheelbaseWidth == obj.wheelbaseWidth &&
			wheelbaseDepth == obj.wheelbaseDepth &&
			drivetrain == obj.drivetrain;
	}
};

// Same as pathfinder settings but parsed/saved  differently
struct OverrideSettings {
	double timestep = -1;
	double velocity = -1;
	double acceleration = -1;
	double jerk = -1;
	std::string fitmethod = "";
	std::string samplerate = "";
	double wheelbaseWidth = -1;
	double wheelbaseDepth = -1;
	std::string drivetrain = "";

	bool operator==(const PathfinderSettings &obj) {
		return timestep == obj.timestep &&
			velocity == obj.velocity &&
			acceleration == obj.acceleration &&
			jerk == obj.jerk &&
			fitmethod == obj.fitmethod &&
			samplerate == obj.samplerate &&
			wheelbaseWidth == obj.wheelbaseWidth &&
			wheelbaseDepth == obj.wheelbaseDepth &&
			drivetrain == obj.drivetrain;
	}
};

struct Angle{
	Angle(double value = 0, std::string name = "", bool named = false) : value(value), angleName(name), namedAngle(named) {}

	double value = 0;
	std::string angleName = "";
	bool namedAngle = false;
};

struct Dimmension{
	Dimmension(double value = 0, std::string name = "", bool measurement = false) : value(value), 
				measurementName(name), measurement(measurement){}
	double value = 0;
	std::string measurementName = "";
	bool measurement = false;
};

struct ReferenceLine{
	Dimmension x1, y1, x2, y2, length;
	Dimmension x1Offset, x2Offset, y1Offset, y2Offset;
	std::string orientation = "";
	bool oriented = false;

	double getStartX(LoadedData &data);
	double getStartY(LoadedData &data);
	double getEndX(LoadedData &data);
	double getEndY(LoadedData &data);
};

struct Waypoint{

	Dimmension x, y, distance;
	Dimmension xOffset, yOffset, reflineOffset, distanceOffset;
	Angle heading, headingOffset;
	std::string reflineName = "";
	bool reflineWaypoint = false;
	bool matchHeading = false;
	bool positiveMatch = true;

	double getX(LoadedData &data);
	double getY(LoadedData &data);
	double getHeading(LoadedData &data);
};

struct Path{
	std::vector<Waypoint> waypoints;
	OverrideSettings overrideSettings;
};

struct LoadedData{
	bool isInMeters = false;
	double width, height;
	std::string unitName;
	PathfinderSettings settings;
	std::unordered_map<std::string, double> measurements;
	std::unordered_map<std::string, double> angles;
	std::unordered_map<std::string, ReferenceLine> reflines;
	std::unordered_map<std::string, Path> paths;

	// Track order of paths
	std::vector<std::string> pathNames;

	double getDimmension(Dimmension d);
	double getAngle(Angle a);
	
	void verifyData();

	LoadedData convertToMeters();
	LoadedData convertToSpecifiedUnit();
};


} // namespace datafile

class Datafile {
public:
	virtual void parse(datafile::LoadedData &data) = 0;
	virtual void save(datafile::LoadedData data) = 0;

protected:
	std::string filepath;

	Datafile(std::string filepath);
};

class YAMLDatafile : public Datafile {
public:
	YAMLDatafile(std::string filePath);

	void parse(datafile::LoadedData &data) override;
	void save(datafile::LoadedData data) override;
};

/**
 * Conversions to/from types defined above for yaml-cpp
 */
namespace YAML {

template<>
struct convert<datafile::PathfinderSettings> {
	static Node encode(const datafile::PathfinderSettings &settings) {
		Node node;
		node[KEY_PATHFINDER_TIMESTEP] = settings.timestep;
		node[KEY_PATHFINDER_VELOCITY] = settings.velocity;
		node[KEY_PATHFINDER_ACCELERATION] = settings.acceleration;
		node[KEY_PATHFINDER_JERK] = settings.jerk;
		node[KEY_PATHFINDER_FITMETHOD] = settings.fitmethod;
		node[KEY_PATHFINDER_SAMPLERATE] = settings.samplerate;
		node[KEY_PATHFINDER_WHEELBASEWIDTH] = settings.wheelbaseWidth;
		node[KEY_PATHFINDER_WHEELBASEDEPTH] = settings.wheelbaseDepth;
		node[KEY_PATHFINDER_DRIVETRAIN] = settings.drivetrain;
		return node;
	}
	static bool decode(const Node &node, datafile::PathfinderSettings &settings) {
		if(!node.IsMap())
			throw ParseException("Pathfinder settings must be an object.");
		if (node[KEY_PATHFINDER_TIMESTEP]) {
			settings.timestep = node[KEY_PATHFINDER_TIMESTEP].as<double>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_TIMESTEP + "'.");
		}
		if (node[KEY_PATHFINDER_VELOCITY]) {
			settings.velocity = node[KEY_PATHFINDER_VELOCITY].as<double>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_VELOCITY + "'.");
		}
		if (node[KEY_PATHFINDER_ACCELERATION]) {
			settings.acceleration = node[KEY_PATHFINDER_ACCELERATION].as<double>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_ACCELERATION + "'.");
		}
		if (node[KEY_PATHFINDER_JERK]) {
			settings.jerk = node[KEY_PATHFINDER_JERK].as<double>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_JERK + "'.");
		}
		if (node[KEY_PATHFINDER_FITMETHOD]) {
			settings.fitmethod = node[KEY_PATHFINDER_FITMETHOD].as<std::string>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_FITMETHOD + "'.");
		}
		if (node[KEY_PATHFINDER_SAMPLERATE]) {
			settings.samplerate = node[KEY_PATHFINDER_SAMPLERATE].as<std::string>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_SAMPLERATE + "'.");
		}
		if (node[KEY_PATHFINDER_WHEELBASEWIDTH]) {
			settings.wheelbaseWidth = node[KEY_PATHFINDER_WHEELBASEWIDTH].as<double>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_WHEELBASEWIDTH + "'.");
		}
		if (node[KEY_PATHFINDER_DRIVETRAIN]) {
			settings.drivetrain = node[KEY_PATHFINDER_DRIVETRAIN].as<std::string>();
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDER_DRIVETRAIN + "'.");
		}
		if (node[KEY_PATHFINDER_WHEELBASEDEPTH]) {
			settings.wheelbaseDepth = node[KEY_PATHFINDER_WHEELBASEDEPTH].as<double>();
		}
		return true;
	}
};

template<>
struct convert<datafile::OverrideSettings>{
	static Node encode(const datafile::OverrideSettings &settings) {
		Node node;
		if(settings.timestep > -1)
			node[KEY_PATHFINDER_TIMESTEP] = settings.timestep;
		if(settings.velocity > -1)
			node[KEY_PATHFINDER_VELOCITY] = settings.velocity;
		if(settings.acceleration > -1)
			node[KEY_PATHFINDER_ACCELERATION] = settings.acceleration;
		if(settings.jerk > -1)
			node[KEY_PATHFINDER_JERK] = settings.jerk;
		if(settings.fitmethod != "")
			node[KEY_PATHFINDER_FITMETHOD] = settings.fitmethod;
		if(settings.samplerate != "")
			node[KEY_PATHFINDER_SAMPLERATE] = settings.samplerate;
		if(settings.wheelbaseWidth > -1)
			node[KEY_PATHFINDER_WHEELBASEWIDTH] = settings.wheelbaseWidth;
		if(settings.wheelbaseDepth > -1)
			node[KEY_PATHFINDER_WHEELBASEDEPTH] = settings.wheelbaseDepth;
		if(settings.drivetrain != "")
			node[KEY_PATHFINDER_DRIVETRAIN] = settings.drivetrain;
		return node;
	}
	static bool decode(const Node &node, datafile::OverrideSettings &settings) {
		if(!node.IsMap())
			throw ParseException("Pathfinder settings must be an object.");
		if (node[KEY_PATHFINDER_TIMESTEP]) {
			settings.timestep = node[KEY_PATHFINDER_TIMESTEP].as<double>();
		} 
		if (node[KEY_PATHFINDER_VELOCITY]) {
			settings.velocity = node[KEY_PATHFINDER_VELOCITY].as<double>();
		}
		if (node[KEY_PATHFINDER_ACCELERATION]) {
			settings.acceleration = node[KEY_PATHFINDER_ACCELERATION].as<double>();
		}
		if (node[KEY_PATHFINDER_JERK]) {
			settings.jerk = node[KEY_PATHFINDER_JERK].as<double>();
		}
		if (node[KEY_PATHFINDER_FITMETHOD]) {
			settings.fitmethod = node[KEY_PATHFINDER_FITMETHOD].as<std::string>();
		} 
		if (node[KEY_PATHFINDER_SAMPLERATE]) {
			settings.samplerate = node[KEY_PATHFINDER_SAMPLERATE].as<std::string>();
		} 
		if (node[KEY_PATHFINDER_WHEELBASEWIDTH]) {
			settings.wheelbaseWidth = node[KEY_PATHFINDER_WHEELBASEWIDTH].as<double>();
		} 
		if (node[KEY_PATHFINDER_DRIVETRAIN]) {
			settings.drivetrain = node[KEY_PATHFINDER_DRIVETRAIN].as<std::string>();
		}
		if (node[KEY_PATHFINDER_WHEELBASEDEPTH]) {
			settings.wheelbaseDepth = node[KEY_PATHFINDER_WHEELBASEDEPTH].as<double>();
		}
		return true;
	}
};

template<>
struct convert<datafile::Dimmension>{
	static Node encode(const datafile::Dimmension &dimmension) {
		Node node;
		if(dimmension.measurement)
			node = dimmension.measurementName;
		else
			node = dimmension.value;
		return node;
	}
	static bool decode(const Node &node, datafile::Dimmension &dimmension) {
		try{
			dimmension.measurement = false;
			dimmension.value = node.as<double>();
		}catch(std::exception &e){
			try{
				dimmension.measurement = true;
				dimmension.measurementName = node.as<std::string>();
			}catch(std::exception &e){
				throw ParseException("Dimmensions must be numeric or strings (names of measurements).");
			}
		}
		return true;
	}
};

template<>
struct convert<datafile::Angle>{
	static Node encode(const datafile::Angle &angle) {
		Node node;
		if(angle.namedAngle)
			node = angle.angleName;
		else
			node = angle.value;
		return node;
	}
	static bool decode(const Node &node, datafile::Angle &angle) {
		try{
			angle.namedAngle = false;
			angle.value = node.as<double>();
		}catch(std::exception &e){
			try{
				angle.namedAngle = true;
				angle.angleName = node.as<std::string>();
			}catch(std::exception &e){
				throw ParseException("Dimmensions must be numeric or strings (named of angles).");
			}
		}
		return true;
	}
};

template<>
struct convert<datafile::ReferenceLine>{
	static Node encode(const datafile::ReferenceLine &refline) {
		Node node;
		if(refline.oriented){
			node[KEY_REFLINE_X] = refline.x1;
			node[KEY_REFLINE_XOFFSET] = refline.x1Offset;
			node[KEY_REFLINE_Y] = refline.y1;
			node[KEY_REFLINE_YOFFSET] = refline.y1Offset;
			node[KEY_REFLINE_LENGTH] = refline.length;
			node[KEY_REFLINE_ORIENTATION] = refline.orientation;
		}else{
			node[KEY_REFLINE_X1] = refline.x1;
			node[KEY_REFLINE_X1OFFSET] = refline.x1Offset;
			node[KEY_REFLINE_Y1] = refline.y1;
			node[KEY_REFLINE_Y1OFFSET] = refline.y1Offset;
			node[KEY_REFLINE_X2] = refline.x2;
			node[KEY_REFLINE_X2OFFSET] = refline.x2Offset;
			node[KEY_REFLINE_Y2] = refline.y2;
			node[KEY_REFLINE_Y2OFFSET] = refline.y2Offset;
		}
		return node;
	}
	static bool decode(const Node &node, datafile::ReferenceLine &refline) {
		if(node[KEY_REFLINE_X] && node[KEY_REFLINE_Y] && node[KEY_REFLINE_LENGTH] && node[KEY_REFLINE_ORIENTATION]){
			refline.oriented = true;
			refline.x1 = node[KEY_REFLINE_X].as<datafile::Dimmension>();
			refline.y1 = node[KEY_REFLINE_Y].as<datafile::Dimmension>();
			refline.length = node[KEY_REFLINE_LENGTH].as<datafile::Dimmension>();
			refline.orientation = node[KEY_REFLINE_ORIENTATION].as<std::string>();

			// Offsets
			if (node[KEY_REFLINE_XOFFSET]) {
				refline.x1Offset = node[KEY_REFLINE_XOFFSET].as<datafile::Dimmension>();
			} else {
				refline.x1Offset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			}
			if (node[KEY_REFLINE_YOFFSET]) {
				refline.y1Offset = node[KEY_REFLINE_YOFFSET].as<datafile::Dimmension>();
			} else {
				refline.y1Offset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			}
			return true;
		}else if(node[KEY_REFLINE_X1] && node[KEY_REFLINE_Y1] && node[KEY_REFLINE_X2] && node[KEY_REFLINE_Y2]){
			refline.oriented = false;
			refline.x1 = node[KEY_REFLINE_X1].as<datafile::Dimmension>();
			refline.y1 = node[KEY_REFLINE_Y1].as<datafile::Dimmension>();
			refline.x2 = node[KEY_REFLINE_X2].as<datafile::Dimmension>();
			refline.y2 = node[KEY_REFLINE_Y2].as<datafile::Dimmension>();

			// Offsets
			if (node[KEY_REFLINE_X1OFFSET]) {
				refline.x1Offset = node[KEY_REFLINE_X1OFFSET].as<datafile::Dimmension>();
			} else {
				refline.x1Offset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			}
			if (node[KEY_REFLINE_Y1OFFSET]) {
				refline.y1Offset = node[KEY_REFLINE_Y1OFFSET].as<datafile::Dimmension>();
			} else {
				refline.y1Offset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			}
			if (node[KEY_REFLINE_X2OFFSET]) {
				refline.x2Offset = node[KEY_REFLINE_X2OFFSET].as<datafile::Dimmension>();
			} else {
				refline.x2Offset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			}
			if (node[KEY_REFLINE_Y2OFFSET]) {
				refline.y2Offset = node[KEY_REFLINE_Y2OFFSET].as<datafile::Dimmension>();
			} else {
				refline.y2Offset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			}
			return true;
		}else{
			throw ParseException("Missing one or more required keys.");
		}
		return true;
	}
};

template<>
struct convert<datafile::Waypoint>{
	static Node encode(const datafile::Waypoint &waypoint) {
		Node node;
		if(waypoint.reflineWaypoint){
			node[KEY_WAYPOINT_REFLINE] = waypoint.reflineName;
			node[KEY_WAYPOINT_DISTANCE] = waypoint.distance;
			node[KEY_WAYPOINT_DISTANCE_OFFSET] = waypoint.distanceOffset;
			node[KEY_WAYPOINT_RLOFFSET] = waypoint.reflineOffset;
			if (waypoint.matchHeading) {
				if (waypoint.positiveMatch) {
					node[KEY_WAYPOINT_HEADING] = "match+";
				} else {
					node[KEY_WAYPOINT_HEADING] = "match-";
				}
			} else {
				node[KEY_WAYPOINT_HEADING] = waypoint.heading;
			}
			node[KEY_WAYPOINT_HEADING_OFFSET] = waypoint.headingOffset;
		}else{
			node[KEY_WAYPOINT_X] = waypoint.x;
			node[KEY_WAYPOINT_XOFFSET] = waypoint.xOffset;
			node[KEY_WAYPOINT_Y] = waypoint.y;
			node[KEY_WAYPOINT_YOFFSET] = waypoint.y;
			node[KEY_WAYPOINT_HEADING] = waypoint.heading;
			node[KEY_WAYPOINT_HEADING] = waypoint.headingOffset;
		}
		return node;
	}
	static bool decode(const Node &node, datafile::Waypoint &waypoint) {
		if(node[KEY_WAYPOINT_REFLINE] && node[KEY_WAYPOINT_DISTANCE] && node[KEY_WAYPOINT_HEADING]){
			waypoint.reflineWaypoint = true;
			waypoint.reflineName = node[KEY_WAYPOINT_REFLINE].as<std::string>();
			waypoint.distance = node[KEY_WAYPOINT_DISTANCE].as<datafile::Dimmension>();

			std::string headingString = node[KEY_WAYPOINT_HEADING].as<std::string>();
			std::transform(headingString.begin(), headingString.end(), headingString.begin(), ::tolower);
			if (headingString == "match+") {
				waypoint.matchHeading = true;
				waypoint.positiveMatch = true;
			} else if (headingString == "match-") {
				waypoint.matchHeading = true;
				waypoint.positiveMatch = false;
			} else {
				waypoint.heading = node[KEY_WAYPOINT_HEADING].as<datafile::Angle>();
			}

			if (node[KEY_WAYPOINT_HEADING_OFFSET]) {
				waypoint.headingOffset = node[KEY_WAYPOINT_HEADING_OFFSET].as<datafile::Angle>();
			} else {
				waypoint.headingOffset = datafile::Angle(0.0, "", false); // Offset of 0 as a datafile::Angle
			}

			if (node[KEY_WAYPOINT_RLOFFSET])
				waypoint.reflineOffset = node[KEY_WAYPOINT_RLOFFSET].as<datafile::Dimmension>();
			else
				waypoint.reflineOffset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
			
			if (node[KEY_WAYPOINT_DISTANCE_OFFSET])
				waypoint.distanceOffset = node[KEY_WAYPOINT_DISTANCE_OFFSET].as<datafile::Dimmension>();
			else
				waypoint.distanceOffset = datafile::Dimmension(0.0, "", false); // Offset of 0 as a datafile::Dimmension
		}else if(node[KEY_WAYPOINT_X] && node[KEY_WAYPOINT_Y] && node[KEY_WAYPOINT_HEADING]){
			waypoint.reflineWaypoint = false;
			waypoint.x = node[KEY_WAYPOINT_X].as<datafile::Dimmension>();
			waypoint.y = node[KEY_WAYPOINT_Y].as<datafile::Dimmension>();
			waypoint.heading = node[KEY_WAYPOINT_HEADING].as<datafile::Angle>();
			if (node[KEY_WAYPOINT_XOFFSET])
				waypoint.xOffset = node[KEY_WAYPOINT_XOFFSET].as<datafile::Dimmension>();
			else
				waypoint.xOffset = { 0.0, "", false }; // Offset of 0 as a datafile::Dimmension
			if (node[KEY_WAYPOINT_YOFFSET])
				waypoint.yOffset = node[KEY_WAYPOINT_YOFFSET].as<datafile::Dimmension>();
			else
				waypoint.yOffset = { 0.0, "", false }; // Offset of 0 as a datafile::Dimmension
		}else{
			throw ParseException("Missing one or more required keys.");
		}
		return true;
	}
};

template<>
struct convert<datafile::Path>{
	static Node encode(const datafile::Path &path) {
		Node node;
		// Cannot just write pathfinder settings because it would write all pathfinder settings (even the ones that weren't overriden)
		if(path.overrideSettings.timestep != -1){
			node[KEY_PATH_OVERRIDESETTINGS][KEY_PATHFINDER_TIMESTEP] = path.overrideSettings.timestep;
		}
		if(path.overrideSettings.velocity != -1){
			node[KEY_PATH_OVERRIDESETTINGS][KEY_PATHFINDER_VELOCITY] = path.overrideSettings.velocity;
		}
		if(path.overrideSettings.acceleration != -1){
			node[KEY_PATH_OVERRIDESETTINGS][KEY_PATHFINDER_ACCELERATION] = path.overrideSettings.acceleration;
		}
		if(path.overrideSettings.jerk != -1){
			node[KEY_PATH_OVERRIDESETTINGS][KEY_PATHFINDER_JERK] = path.overrideSettings.jerk;
		}
		if(path.overrideSettings.fitmethod != ""){
			node[KEY_PATH_OVERRIDESETTINGS][KEY_PATHFINDER_FITMETHOD] = path.overrideSettings.fitmethod;
		}
		if(path.overrideSettings.samplerate != ""){
			node[KEY_PATH_OVERRIDESETTINGS][KEY_PATHFINDER_SAMPLERATE] = path.overrideSettings.samplerate;
		}
		// TODO: Write all waypoints
		throw "NOT WRITING WAYPOINTS!!!";
		return node;
	}
	static bool decode(const Node &node, datafile::Path &path) {
		if(node[KEY_PATH_OVERRIDESETTINGS]){
			path.overrideSettings = node[KEY_PATH_OVERRIDESETTINGS].as<datafile::OverrideSettings>();
		}
		if(node[KEY_PATH_WAYPOINTS]){
			for(size_t i = 0; i < node[KEY_PATH_WAYPOINTS].size(); ++i){
				path.waypoints.push_back(node[KEY_PATH_WAYPOINTS][i].as<datafile::Waypoint>());
			}
		}else{
			throw ParseException("Missing key '" + std::string(KEY_PATH_WAYPOINTS) + "'.");
		}
		return true;
	}
};

template<>
struct convert<datafile::LoadedData>{
	static Node encode(const datafile::LoadedData &path) {
		Node node;
		
		return node;
	}
	static bool decode(const Node &node, datafile::LoadedData &data) {
		// Will not modify data yet
		data.isInMeters = false;
		// Load width
		if (node[KEY_WIDTH]) {
			try {
				data.width = node[KEY_WIDTH].as<double>();
			} catch (std::exception &e) {
				throw ParseException(std::string("Error parsing width setting.\n") + e.what());
			}
		} else {
			throw ParseException(std::string("Missing key '") + KEY_WIDTH + "'.");
		}

		// Load unit
		if (node[KEY_UNIT]) {
			try {
				data.unitName = node[KEY_UNIT].as<std::string>();
			} catch (std::exception &e) {
				throw ParseException(std::string("Error parsing unit setting.\n") + e.what());
			}
		} else {
			throw ParseException(std::string("Missing key '") + KEY_UNIT + "'.");
		}


		// Load height
		if (node[KEY_HEIGHT]) {
			try {
				data.height = node[KEY_HEIGHT].as<double>();
			} catch (std::exception &e) {
				throw ParseException(std::string("Error parsing height setting.\n") + e.what());
			}
		} else {
			throw ParseException(std::string("Missing key '") + KEY_HEIGHT + "'.");
		}

		// Load pathfinder settings
		if (node[KEY_PATHFINDERSETTINGS]) {
			try {
				data.settings = node[KEY_PATHFINDERSETTINGS].as<datafile::PathfinderSettings>();
			} catch (std::exception &e) {
				throw ParseException(std::string("Error parsing pathfinder settings.\n") + e.what());
			}
		} else {
			throw ParseException(std::string("Missing key '") + KEY_PATHFINDERSETTINGS + "'.");
		}

		// Load measurements
		if (node[KEY_MEASUREMENTS] && !node[KEY_MEASUREMENTS].IsNull()) {
			if (!node[KEY_MEASUREMENTS].IsSequence())
				throw ParseException("'" + std::string(KEY_MEASUREMENTS) + "' must be an array.");
			size_t i;
			try {
				for (i = 0; i < node[KEY_MEASUREMENTS].size(); ++i) {
					auto it = node[KEY_MEASUREMENTS][i].begin(); // Each measurement is a map with 1 key and value
					std::string key = it->first.as<std::string>();
					double value = it->second.as<double>();
					if (data.measurements.find(key) != data.measurements.end())
						throw ParseException(std::string("Measurement with name '") + key + "' already exists.");
					data.measurements[key] = value;
				}
			} catch (std::exception &e) {
				throw ParseException(std::string("Error parsing Measurement #" + std::to_string(i + 1) + ".\n") + e.what());
			}
		} else if (!node[KEY_MEASUREMENTS].IsNull()) {
			throw ParseException(std::string("Missing key '") + KEY_MEASUREMENTS + "'.");
		}

		// Load angles
		if (node[KEY_ANGLES] && !node[KEY_ANGLES].IsNull()) {
			if (!node[KEY_ANGLES].IsSequence())
				throw ParseException("'" + std::string(KEY_ANGLES) + "' must be an array.");
			size_t i;
			try {
				for (i = 0; i < node[KEY_ANGLES].size(); ++i) {
					auto it = node[KEY_ANGLES][i].begin(); // Each angle is a map with 1 key and value
					std::string key = it->first.as<std::string>();
					double value = it->second.as<double>();
					if (data.angles.find(key) != data.angles.end())
						throw ParseException(std::string("Angle with name '") + key + "' already exists.");
					data.angles[key] = value;
				}
			} catch (std::exception &e) {
				throw ParseException(std::string("Error parsing Angle #" + std::to_string(i + 1) + ".\n") + e.what());
			}
		} else if(!node[KEY_ANGLES].IsNull()){
			throw ParseException(std::string("Missing key '") + KEY_ANGLES + "'.");
		}

		// Load reference lines
		if (node[KEY_REFLINES] && !node[KEY_REFLINES].IsNull()) {
			if (!node[KEY_REFLINES].IsSequence())
				throw ParseException("'" + std::string(KEY_REFLINES) + "' must be an array.");
			size_t i;
			std::string rlName = "";
			try {
				for (i = 0; i < node[KEY_REFLINES].size(); ++i) {
					auto it = node[KEY_REFLINES][i].begin(); // Each refline is a map
					std::string key = it->first.as<std::string>();
					rlName = key;
					datafile::ReferenceLine value = it->second.as<datafile::ReferenceLine>();
					if (data.reflines.find(key) != data.reflines.end())
						throw ParseException(std::string("Reference line with name '") + key + "' already exists.");
					data.reflines[key] = value;
				}
			} catch (std::exception &e) {
				std::string id;
				if (rlName == "")
					id = "reference line #" + std::to_string(i);
				else
					id = "reference line named '" + rlName + "'";
				throw ParseException(std::string("Error parsing " + id + ".\n") + e.what());
			}
		} else if (!node[KEY_REFLINES].IsNull()) {
			throw ParseException(std::string("Missing key '") + KEY_ANGLES + "'.");
		}

		if (node[KEY_PATHS] && !node[KEY_PATHS].IsNull()) {
			if (!node[KEY_PATHS].IsSequence())
				throw ParseException("'" + std::string(KEY_PATHS) + "' must be an array.");
			size_t i;
			std::string pathName = "";
			try {
				for (i = 0; i < node[KEY_PATHS].size(); ++i) {
					auto it = node[KEY_PATHS][i].begin(); // Each refline is a map
					std::string key = it->first.as<std::string>();
					pathName = key;
					datafile::Path value = it->second.as<datafile::Path>();
					if (data.paths.find(key) != data.paths.end())
						throw ParseException(std::string("Path with name '") + key + "' already exists.");
					data.pathNames.push_back(key);
					data.paths[key] = value;
				}
			} catch (std::exception &e) {
				std::string id;
				if (pathName == "")
					id = "path #" + std::to_string(i);
				else
					id = "path named '" + pathName + "'";
				throw ParseException(std::string("Error parsing " + id + ".\n") + e.what());
			}
		} else if (!node[KEY_PATHS].IsNull()){
			throw ParseException(std::string("Missing key '") + KEY_ANGLES + "'.");
		}

		return true;
	}
};


} // namespace YAML