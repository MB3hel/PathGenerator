#pragma once

#include <PathGenratorUI.h>

#define HORIZ 0
#define VERT 1
#define CUSTOM 2

class RefLineView : public RefLineViewBase {
public:
	RefLineView(wxWindow *parent);

private:
	void updateShownItems();
	void typeSelected(wxCommandEvent &event) override;
};

class DatafileGUI : public DatafileGUIBase {
public:
	DatafileGUI(wxWindow *parent);
};