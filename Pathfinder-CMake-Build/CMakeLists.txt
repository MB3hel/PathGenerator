# Custom CMake build system to build pathfinder library

include_directories(${PROJECT_SOURCE_DIR}/Pathfinder/Pathfinder-Core/include)
file(GLOB_RECURSE SOURCES
    "${PROJECT_SOURCE_DIR}/Pathfinder/Pathfinder-Core/src/*.c"
    "${PROJECT_SOURCE_DIR}/Pathfinder/Pathfinder-Core/include/*.h"
)
add_library(PathfinderCustom STATIC ${SOURCES})